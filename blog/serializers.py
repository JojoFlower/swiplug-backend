from .models import Article
from rest_framework import serializers


class ArticleListSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("id", "title", "picture")
        model = Article


class ArticleDetailSerializer(ArticleListSerializer):
    class Meta(ArticleListSerializer.Meta):
        fields = ArticleListSerializer.Meta.fields + ("content",)
