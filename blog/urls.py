from django.urls import include, path
from .views import ArticleViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r"articles", ArticleViewSet, basename="ArticleViewSet")

urlpatterns = [path("", include(router.urls))]
