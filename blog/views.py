from rest_framework.viewsets import ReadOnlyModelViewSet
from .serializers import ArticleListSerializer, ArticleDetailSerializer
from .models import Article


class ArticleViewSet(ReadOnlyModelViewSet):
    queryset = Article.objects.all().order_by("-updated_at")

    def get_serializer_class(self):
        if self.action == "list":
            return ArticleListSerializer
        else:
            return ArticleDetailSerializer
