from django.contrib import admin
from .models import (
    Item,
    ItemLessee,
    ItemPicture,
    ItemCategory,
    ItemSubCategory,
    ItemLocation,
    ItemSubLocation,
)


class ItemAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "slug", "creator", "is_item_sold", "created_at", "updated_at")


class ItemPictureAdmin(admin.ModelAdmin):
    list_display = ("id", "picture", "item", "created_at", "updated_at")


class ItemCategoryAdmin(admin.ModelAdmin):
    list_display = ("name_en", "name_fr")


class ItemSubCategoryAdmin(admin.ModelAdmin):
    list_display = ("category", "name_en", "name_fr", "order")


class ItemLocationAdmin(admin.ModelAdmin):
    list_display = ("name_en", "name_fr")


class ItemSubLocationAdmin(admin.ModelAdmin):
    list_display = ("location", "name_en", "name_fr", "order")


class ItemLesseeAdmin(admin.ModelAdmin):
    list_display = ("item", "lessee", "from_date", "to_date")


admin.site.register(Item, ItemAdmin)
admin.site.register(ItemPicture, ItemPictureAdmin)
admin.site.register(ItemCategory, ItemCategoryAdmin)
admin.site.register(ItemSubCategory, ItemSubCategoryAdmin)
admin.site.register(ItemLocation, ItemLocationAdmin)
admin.site.register(ItemSubLocation, ItemSubLocationAdmin)
admin.site.register(ItemLessee, ItemLesseeAdmin)
