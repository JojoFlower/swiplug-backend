from django.contrib.postgres.operations import TrigramExtension
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [("item", "0004_auto_20190505_1319")]

    operations = [TrigramExtension()]
