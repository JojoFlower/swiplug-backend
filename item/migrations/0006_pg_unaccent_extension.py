from django.contrib.postgres.operations import UnaccentExtension
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [("item", "0005_pg_trigram_extension")]

    operations = [UnaccentExtension()]
