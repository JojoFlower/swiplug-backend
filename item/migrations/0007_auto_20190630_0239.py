# Generated by Django 2.2.2 on 2019-06-30 02:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("item", "0006_pg_unaccent_extension")]

    operations = [
        migrations.CreateModel(
            name="ItemCategory",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name_en", models.CharField(max_length=255)),
                ("name_fr", models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name="ItemSubCategory",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name_en", models.CharField(max_length=255)),
                ("name_fr", models.CharField(max_length=255)),
                (
                    "category",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="subcategories",
                        to="item.ItemCategory",
                    ),
                ),
            ],
        ),
        migrations.AlterField(
            model_name="item",
            name="category",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="items",
                to="item.ItemSubCategory",
            ),
        ),
    ]
