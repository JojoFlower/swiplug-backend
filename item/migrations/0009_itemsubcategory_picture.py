# Generated by Django 2.2.2 on 2019-07-08 13:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("item", "0008_itemsubcategory_order")]

    operations = [
        migrations.AddField(
            model_name="itemsubcategory",
            name="picture",
            field=models.ImageField(
                blank=True, null=True, upload_to="subcategory_pics"
            ),
        )
    ]
