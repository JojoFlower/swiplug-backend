# Generated by Django 2.2.2 on 2019-07-14 07:57

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [("item", "0009_itemsubcategory_picture")]

    operations = [
        migrations.AddField(
            model_name="item",
            name="from_time",
            field=models.TimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="item",
            name="price_month",
            field=models.DecimalField(
                blank=True, decimal_places=10, max_digits=15, null=True
            ),
        ),
        migrations.AddField(
            model_name="item",
            name="price_week",
            field=models.DecimalField(
                blank=True, decimal_places=10, max_digits=15, null=True
            ),
        ),
        migrations.AddField(
            model_name="item",
            name="pricing",
            field=models.CharField(
                blank=True,
                choices=[("day", "day"), ("week", "week"), ("month", "month")],
                max_length=32,
            ),
        ),
        migrations.AddField(
            model_name="item",
            name="return_address",
            field=models.CharField(default="Québec", max_length=512),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="item",
            name="to_time",
            field=models.TimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="item",
            name="price",
            field=models.DecimalField(
                blank=True, decimal_places=10, max_digits=15, null=True
            ),
        ),
    ]
