# Generated by Django 2.2.4 on 2019-08-19 18:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("item", "0017_item_customer")]

    operations = [
        migrations.AddField(
            model_name="item",
            name="is_item_received",
            field=models.BooleanField(default=False),
        )
    ]
