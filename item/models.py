from django.db import models
from django.db.models import Avg
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from rest_framework.exceptions import ValidationError
from django.db.models.signals import pre_save, pre_delete

User = get_user_model()


class ItemCategory(models.Model):
    name_en = models.CharField(max_length=255)
    name_fr = models.CharField(max_length=255)

    def __str__(self):
        return self.name_en


class ItemSubCategory(models.Model):
    category = models.ForeignKey(
        ItemCategory, related_name="subcategories", on_delete=models.CASCADE
    )
    name_en = models.CharField(max_length=255)
    name_fr = models.CharField(max_length=255)
    picture = models.ImageField(upload_to="subcategory_pics", blank=True, null=True)
    order = models.PositiveSmallIntegerField(blank=True, null=True)

    def __str__(self):
        return f"{self.name_en}__{str(self.category)}"


class ItemLocation(models.Model):
    name_en = models.CharField(max_length=255)
    name_fr = models.CharField(max_length=255)

    def __str__(self):
        return self.name_en


class ItemSubLocation(models.Model):
    location = models.ForeignKey(
        ItemLocation, related_name="sublocations", on_delete=models.CASCADE
    )
    name_en = models.CharField(max_length=255)
    name_fr = models.CharField(max_length=255)
    order = models.PositiveSmallIntegerField(blank=True, null=True)

    def __str__(self):
        return f"{self.name_en}__{str(self.location)}"


class Item(models.Model):
    category = models.ForeignKey(
        ItemSubCategory, related_name="items", on_delete=models.CASCADE
    )
    location = models.ForeignKey(
        ItemSubLocation, related_name="items", on_delete=models.CASCADE
    )
    title = models.CharField(max_length=127)
    slug = models.SlugField(max_length=127, unique=True)
    description = models.TextField()

    # Purchase
    charge_id = models.CharField(max_length=255, blank=True)  # charge id for payment
    price = models.DecimalField(
        max_digits=15, decimal_places=10, blank=True, null=True
    )  # buying price

    # Rental
    price_day = models.DecimalField(
        max_digits=15, decimal_places=10, blank=True, null=True
    )  # daily

    # Address
    address = models.CharField(max_length=512)  # pick-up address
    return_address = models.CharField(max_length=512, blank=True)  # return address

    creator = models.ForeignKey(
        User, related_name="owned_items", on_delete=models.CASCADE
    )

    customer = models.ForeignKey(
        User,
        related_name="purchased_items",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    # Rental availability
    from_date = models.DateField(null=True, blank=True)
    to_date = models.DateField(null=True, blank=True)
    from_time = models.TimeField(null=True, blank=True)
    to_time = models.TimeField(null=True, blank=True)

    is_item_received = models.BooleanField(default=False)
    is_item_sold = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.creator}-{self.slug}"

    def save(self, *args, **kwargs):
        if not (self.price or self.price_day):
            raise ValidationError({"pricing": ["At least one pricing must be added"]})
        if (self.price and self.price < 0) or (self.price_day and self.price_day < 0):
            raise ValidationError({"pricing": ["Price must be positive"]})
        super().save(*args, **kwargs)

    @property
    def category_name_en(self):
        return self.category.name_en

    @property
    def super_category(self):
        return self.category.category.id

    @property
    def super_category_name(self):
        return self.category.category.name_en

    @property
    def location_name_en(self):
        return self.location.name_en

    @property
    def customer_firstname(self):
        return self.customer.first_name

    @property
    def customer_email(self):
        return self.customer.email

    @property
    def customer_phone_number(self):
        return self.customer.phone_number

    @property
    def mark(self):
        return {
            "communication_mark": self.creator.user_ratings.aggregate(
                Avg("communication_mark")
            )["communication_mark__avg"],
            "item_condition_mark": self.creator.user_ratings.aggregate(
                Avg("item_condition_mark")
            )["item_condition_mark__avg"],
            "payment_equity_mark": self.creator.user_ratings.aggregate(
                Avg("payment_equity_mark")
            )["payment_equity_mark__avg"],
            "timeliness_mark": self.creator.user_ratings.aggregate(
                Avg("timeliness_mark")
            )["timeliness_mark__avg"],
        }

    @property
    def creator_firstname(self):
        return self.creator.first_name

    @property
    def creator_email(self):
        return self.creator.email

    @property
    def creator_phone(self):
        return self.creator.phone_number

    @property
    def creator_picture(self):
        return (
            self.creator.profile_picture.url
            if (self.creator and self.creator.profile_picture)
            else None
        )

    @property
    def customer_picture(self):
        return (
            self.customer.profile_picture.url
            if (self.customer and self.customer.profile_picture)
            else None
        )


class ItemLessee(models.Model):
    item = models.ForeignKey(
        Item, related_name="item_lessees", on_delete=models.CASCADE
    )
    lessee = models.ForeignKey(
        User, related_name="item_lessees", on_delete=models.CASCADE
    )
    from_date = models.DateField()
    to_date = models.DateField()

    class Meta:
        unique_together = ("item", "lessee")

    def __str__(self):
        return f"{self.lessee}-{self.item.slug}"

    @property
    def lessee_firstname(self):
        return self.lessee.first_name

    @property
    def mark(self):
        return {
            "communication_mark": self.lessee.user_ratings.aggregate(
                Avg("communication_mark")
            )["communication_mark__avg"],
            "item_condition_mark": self.lessee.user_ratings.aggregate(
                Avg("item_condition_mark")
            )["item_condition_mark__avg"],
            "payment_equity_mark": self.lessee.user_ratings.aggregate(
                Avg("payment_equity_mark")
            )["payment_equity_mark__avg"],
            "timeliness_mark": self.lessee.user_ratings.aggregate(
                Avg("timeliness_mark")
            )["timeliness_mark__avg"],
        }

    @property
    def lessee_email(self):
        return self.lessee.email

    @property
    def lessee_phone_number(self):
        return self.lessee.phone_number

    @property
    def lessee_picture(self):
        return (
            self.lessee.profile_picture.url
            if (self.lessee and self.lessee.profile_picture)
            else None
        )

    def save(self, *args, **kwargs):
        bookings = self.item.item_lessees.all()

        for booking in bookings:
            if self.from_date <= booking.from_date and self.to_date >= booking.to_date:
                raise ValidationError({"timespan": ["Dates not valid"]})

        super().save(*args, **kwargs)


class ItemPicture(models.Model):
    item = models.ForeignKey(Item, related_name="pictures", on_delete=models.CASCADE)
    picture = models.ImageField(upload_to="item_pics")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.picture.url


@receiver(pre_save, sender=ItemPicture)
def delete_s3_image_save(sender, instance, **kwargs):
    if sender.objects.filter(pk=instance.pk).exists():
        obj = sender.objects.get(pk=instance.pk).picture
        if obj and obj != instance.picture:
            storage, name = (obj.storage, obj.name)
            storage.delete(name)


@receiver(pre_delete, sender=ItemPicture)
def delete_s3_image_delete(sender, instance, **kwargs):
    obj = instance.picture
    if obj:
        storage, name = (obj.storage, obj.name)
        storage.delete(name)


@receiver(pre_save, sender=ItemSubCategory)
def delete_s3_category_save(sender, instance, **kwargs):
    if sender.objects.filter(pk=instance.pk).exists():
        obj = sender.objects.get(pk=instance.pk).picture
        if obj and obj != instance.picture:
            storage, name = (obj.storage, obj.name)
            storage.delete(name)


@receiver(pre_delete, sender=ItemSubCategory)
def delete_s3_category_delete(sender, instance, **kwargs):
    obj = instance.picture
    if obj:
        storage, name = (obj.storage, obj.name)
        storage.delete(name)
