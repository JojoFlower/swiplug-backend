from rest_framework import serializers
from .models import (
    Item,
    ItemLessee,
    ItemPicture,
    ItemCategory,
    ItemSubCategory,
    ItemLocation,
    ItemSubLocation,
)


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("id", "name_en", "name_fr", "picture")
        model = ItemSubCategory


class CategorySerializer(serializers.ModelSerializer):
    subcategories = serializers.SerializerMethodField()

    class Meta:
        fields = ("id", "name_en", "name_fr", "subcategories")
        model = ItemCategory

    def get_subcategories(self, instance):
        subcategories = instance.subcategories.all().order_by("order")
        return SubCategorySerializer(subcategories, many=True).data


class SubLocationSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("id", "name_en", "name_fr")
        model = ItemSubLocation


class LocationSerializer(serializers.ModelSerializer):
    sublocations = serializers.SerializerMethodField()

    class Meta:
        fields = ("id", "name_en", "name_fr", "sublocations")
        model = ItemLocation

    def get_sublocations(self, instance):
        sublocations = instance.sublocations.all().order_by("order")
        return SubLocationSerializer(sublocations, many=True).data


class ItemListSerializer(serializers.ModelSerializer):
    pictures = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        fields = (
            "id",
            "category",
            "super_category",
            "super_category_name",
            "category_name_en",
            "location",
            "location_name_en",
            "title",
            "slug",
            "price",
            "price_day",
            "from_date",
            "to_date",
            "address",
            "return_address",
            "customer",
            "pictures",
            "from_time",
            "to_time",
            "is_item_sold",
            "updated_at",
            "created_at",
        )
        model = Item
        read_only_fields = ("slug", "customer")
        extra_kwargs = {"slug": {"required": False}}


class ItemDetailSerializer(ItemListSerializer):
    class Meta(ItemListSerializer.Meta):
        fields = ItemListSerializer.Meta.fields + (
            "description",
            "creator",
            "customer_firstname",
            "customer_picture",
            "creator_firstname",
            "creator_phone",
            "creator_email",
            "creator_picture",
            "customer_phone_number",
            "customer_email",
            "is_item_received",
            "mark",
        )
        read_only_fields = ("creator",)


class ItemLesseeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("id", "item", "lessee", "from_date", "to_date", "mark")
        model = ItemLessee
        read_only_fields = ("lessee",)


class ItemLesseeOwnerSerializer(ItemLesseeSerializer):
    class Meta(ItemLesseeSerializer.Meta):
        fields = ItemLesseeSerializer.Meta.fields + (
            "lessee_firstname",
            "lessee_email",
            "lessee_phone_number",
            "lessee_picture",
        )


class ItemPictureSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("id", "item", "picture", "created_at", "updated_at")
        model = ItemPicture
