from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string


def item_first_offer_notification(creator, title):
    message = render_to_string(
        f"{settings.BASE_DIR}/templates/notifications/item_first_offer.html",
        {
            "firstname": creator.first_name,
            "title": title,
            "url": f"{settings.FRONTEND_URL}/settings",
        },
    )

    send_mail(
        subject="[Swiplug, Mkrt.] First Offer Submitted",
        message=f"{title} has been submitted",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[creator.email],
        fail_silently=False,
        html_message=message,
    )


def item_booking_notification(itemLessee):
    message = render_to_string(
        f"{settings.BASE_DIR}/templates/notifications/item_booking_swiper.html",
        {
            "firstname": itemLessee.lessee.first_name,
            "title": itemLessee.item.title,
            "from_date": itemLessee.from_date,
            "to_date": itemLessee.to_date,
            "url": f"{settings.FRONTEND_URL}/orders",
            "price": round(
                (itemLessee.to_date - itemLessee.from_date).days
                * itemLessee.item.price_day,
                2,
            ),
        },
    )

    message2 = render_to_string(
        f"{settings.BASE_DIR}/templates/notifications/item_booking_pluger.html",
        {
            "firstname": itemLessee.item.creator.first_name,
            "title": itemLessee.item.title,
            "from_date": itemLessee.from_date,
            "to_date": itemLessee.to_date,
            "url": f"{settings.FRONTEND_URL}/offers",
            "price": round(
                (itemLessee.to_date - itemLessee.from_date).days
                * itemLessee.item.price_day,
                2,
            ),
        },
    )

    for recipient in [itemLessee.lessee.email, settings.EMAIL_TRANSACTION_TRACKER]:
        send_mail(
            subject="[Swiplug, Mkrt.] Booking Completed",
            message=f"{itemLessee.item.title}'s have been booked",
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[recipient],
            fail_silently=False,
            html_message=message,
        )

    for recipient in [itemLessee.item.creator.email, settings.EMAIL_TRANSACTION_TRACKER]:
        send_mail(
            subject="[Swiplug, Mkrt.] Booking Received",
            message=f"{itemLessee.item.title}'s have been booked",
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[recipient],
            fail_silently=False,
            html_message=message2,
        )


def item_payment_credits(user, credit, price):
    message = render_to_string(
        f"{settings.BASE_DIR}/templates/notifications/item_payment_credits.html",
        {
            "firstname": user.first_name,
            "price": round(price, 2),
            "credit": credit,
            "url": f"{settings.FRONTEND_URL}",
        },
    )

    send_mail(
        subject="[Swiplug, Mkrt.] Purchased Credits",
        message=f"Booking Credits have been purchased",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        fail_silently=False,
        html_message=message,
    )


def item_payment_store(item):
    message_swiper = render_to_string(
        f"{settings.BASE_DIR}/templates/notifications/item_payment_store_swiper.html",
        {
            "firstname": item.customer_firstname,
            "title": item.title,
            "price": round(item.price, 2),
            "url": f"{settings.FRONTEND_URL}/orders",
        },
    )

    for recipient in [item.customer.email, settings.EMAIL_TRANSACTION_TRACKER]:
        send_mail(
            subject="[Swiplug, Mkrt.] Purchased Item",
            message=f"{item.title} has been purchased",
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[recipient],
            fail_silently=False,
            html_message=message_swiper,
        )

    message_pluger = render_to_string(
        f"{settings.BASE_DIR}/templates/notifications/item_payment_store_pluger.html",
        {
            "firstname": item.creator.first_name,
            "title": item.title,
            "price": round(item.price, 2),
            "url": f"{settings.FRONTEND_URL}/offers",
        },
    )

    for recipient in [item.creator.email, settings.EMAIL_TRANSACTION_TRACKER]:
        send_mail(
            subject="[Swiplug, Mkrt.] Sold Offer",
            message=f"{item.title} has been sold",
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[recipient],
            fail_silently=False,
            html_message=message_pluger,
        )


def item_transfert_funds(item):
    message = render_to_string(
        f"{settings.BASE_DIR}/templates/notifications/item_transfert_funds_pluger.html",
        {
            "firstname": item.creator.first_name,
            "title": item.title,
            "price": round(item.price, 2),
            "url": f"{settings.FRONTEND_URL}/settings",
        },
    )

    message2 = render_to_string(
        f"{settings.BASE_DIR}/templates/notifications/item_transfert_funds_swiper.html",
        {
            "firstname": item.customer.first_name,
            "title": item.title,
            "price": round(item.price, 2),
            "url": f"{settings.FRONTEND_URL}/orders",
        },
    )

    for recipient in [item.creator.email, settings.EMAIL_TRANSACTION_TRACKER]:
        send_mail(
            subject="[Swiplug, Mkrt.] Received funds",
            message=f"{item.title}'s funds have been tranfered",
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[recipient],
            fail_silently=False,
            html_message=message,
        )

    for recipient in [item.customer.email, settings.EMAIL_TRANSACTION_TRACKER]:
        send_mail(
            subject="[Swiplug, Mkrt.] Transfered funds",
            message=f"{item.title}'s funds have been tranfered",
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[recipient],
            fail_silently=False,
            html_message=message2,
        )
