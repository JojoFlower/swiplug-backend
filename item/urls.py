from rest_framework import routers
from .views import (
    ItemViewSet,
    ItemPictureViewSet,
    CategoryReadOnlyViewSet,
    LocationReadOnlyViewSet,
    ItemLesseeViewSet,
)

router = routers.DefaultRouter()
router.register(r"items", ItemViewSet, basename="ItemViewSet")
router.register(r"lessee-items", ItemLesseeViewSet, basename="ItemLesseeViewSet")
router.register(r"item-pictures", ItemPictureViewSet, basename="ItemPictureViewSet")
router.register(
    r"categories", CategoryReadOnlyViewSet, basename="CategoryReadOnlyViewSet"
)
router.register(
    r"locations", LocationReadOnlyViewSet, basename="LocationReadOnlyViewSet"
)
urlpatterns = router.urls
