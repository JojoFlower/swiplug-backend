import uuid
import stripe
import bleach
from django.conf import settings
from .models import Item, ItemPicture, ItemCategory, ItemLocation, ItemLessee
from .service import (
    item_first_offer_notification,
    item_payment_store,
    item_transfert_funds,
    item_payment_credits,
    item_booking_notification,
)

from django.utils.text import slugify
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.pagination import PageNumberPagination
from django.db.models import Q
from .serializers import (
    ItemLesseeSerializer,
    CategorySerializer,
    ItemListSerializer,
    ItemPictureSerializer,
    LocationSerializer,
    ItemDetailSerializer,
    ItemLesseeOwnerSerializer,
)


class CategoryReadOnlyViewSet(ReadOnlyModelViewSet):
    serializer_class = CategorySerializer

    def get_queryset(self):
        items = []
        for item in ItemCategory.objects.all().order_by("subcategories__order"):
            if item not in items:
                items.append(item)
        return items


class LocationReadOnlyViewSet(ReadOnlyModelViewSet):
    serializer_class = LocationSerializer

    def get_queryset(self):
        locations = []
        for location in ItemLocation.objects.all().order_by("sublocations__order"):
            if location not in locations:
                locations.append(location)
        return locations


class ItemPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        if (
            request.method in ["POST", "PUT", "PATCH", "DELETE"]
            and request.user
            and request.user.is_authenticated
        ):
            return True

        return False

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user == obj.creator and obj.customer is None:
            return True

        return False


class ItemLesseePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user and request.user.is_authenticated:
            if request.method in ["POST"]:
                return True
        return False


class ItemPicturePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        if (
            request.method in ["POST", "DELETE"]
            and request.user
            and request.user.is_authenticated
        ):
            return True

        return False

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user == obj.item.creator and obj.item.customer is None:
            return True

        return False


class ItemPagination(PageNumberPagination):
    page_size = 8


class ItemViewSet(ModelViewSet):
    permission_classes = (ItemPermission,)
    pagination_class = ItemPagination
    lookup_field = "slug"

    def get_queryset(self):
        if (
            self.request.user
            and self.request.user.is_authenticated
            and self.request.method == "GET"
        ):
            if "owned" in self.request.GET:
                return self.request.user.owned_items.all()
            if "purchased" in self.request.GET:
                item_lessee_ids = self.request.user.item_lessees.all().values_list(
                    "item"
                )
                return Item.objects.filter(
                    Q(pk__in=item_lessee_ids) | Q(customer=self.request.user)
                )
            if "active_offers" in self.request.GET:
                return self.request.user.owned_items.filter(customer=None).order_by(
                    "-updated_at"
                )
            if "paid_offers" in self.request.GET:
                return (
                    self.request.user.owned_items.exclude(customer=None)
                    .filter(is_item_received=False)
                    .order_by("-updated_at")
                )
            if "payment_received" in self.request.GET:
                return (
                    self.request.user.owned_items.exclude(customer=None)
                    .filter(is_item_received=True)
                    .order_by("-updated_at")
                )
            if "bought_offers" in self.request.GET:
                return self.request.user.purchased_items.filter(
                    is_item_received=False
                ).order_by("-updated_at")
            if "reception_confirm" in self.request.GET:
                return self.request.user.purchased_items.filter(
                    is_item_received=True
                ).order_by("-updated_at")

        if (
            "title" in self.request.GET
            or "category" in self.request.GET
            or "location" in self.request.GET
        ):
            items = (
                Item.objects.filter(
                    Q(title__unaccent__icontains=self.request.GET["title"])
                    | Q(title__unaccent__trigram_similar=self.request.GET["title"])
                )
                .filter(customer=None)
                .order_by("-updated_at")
            )
            category = self.request.GET.get("category", None)
            location = self.request.GET.get("location", None)
            if category:
                if "s" in category:
                    items = items.filter(
                        category__category__id=category.replace("s", "")
                    )
                else:
                    items = items.filter(category_id=category)
            if location:
                if "s" in location:
                    items = items.filter(
                        location__location__id=location.replace("s", "")
                    )
                else:
                    items = items.filter(location_id=location)
            return items

        return Item.objects.filter(customer=None).order_by("-updated_at")

    def get_serializer_class(self):
        if self.action == "list":
            return ItemListSerializer
        else:
            return ItemDetailSerializer

    def perform_create(self, serializer):
        creator = self.request.user
        slug = unique_slug_generator(self.request.data["title"])
        description = bleach.clean(self.request.data["description"]).replace(
            "\n", "<br />"
        )  # prevent xss for dangerousInnerHtml
        if not creator.is_first_offer_submit:
            creator.is_first_offer_submit = True
            creator.save()
            item_first_offer_notification(creator, self.request.data["title"])
        serializer.save(slug=slug, creator=creator, description=description)

    def perform_update(self, serializer):
        description = bleach.clean(self.request.data["description"]).replace(
            "\n", "<br />"
        )
        serializer.save(description=description)

    @action(detail=True, methods=["post"], permission_classes=(IsAuthenticated,))
    def payment(self, request, slug=None):
        if slug != "default":
            instance = get_object_or_404(Item.objects.filter(customer=None), slug=slug)

        if request.data["pricing"] == "rental":
            stripe.api_key = settings.STRIPE_PRIVATE_KEY
            credit = int(request.data["credits"])
            tax = int(request.data["tax"])
            if credit == 1:
                amount = int(4.49 * (1 + tax / 100) * 100)
            elif credit == 2:
                amount = int(8.75 * (1 + tax / 100) * 100)
            elif credit == 5:
                amount = int(19.99 * (1 + tax / 100) * 100)
            elif credit == 8:
                amount = int(29.99 * (1 + tax / 100) * 100)
            elif credit == 15:
                amount = int(49.99 * (1 + tax / 100) * 100)
            charge = stripe.Charge.create(
                amount=amount, currency="cad", source=request.data["payment_token"]
            )

            if charge:
                user = self.request.user
                user.booking_credits += credit
                user.save()
                item_payment_credits(user, credit, amount / 100)
            else:
                raise ValidationError(
                    {"payment": ["Something went wrong with the payment"]}
                )

            return Response({"detail": "Credits added"})

        if request.data["pricing"] == "sale":
            customer = self.request.user

            if customer == instance.creator:
                raise ValidationError(
                    {"customer": ["The owner cannot buy his own plug"]}
                )
            instance.customer = customer

            stripe.api_key = settings.STRIPE_PRIVATE_KEY
            amount = int(instance.price * 100)

            if int(float(request.data["price"]) * 100) == amount:
                charge = stripe.Charge.create(
                    amount=amount, currency="cad", source=request.data["payment_token"]
                )

            if charge:
                item_lessees = instance.item_lessees.all()
                for item_lessee in item_lessees:
                    lessee = item_lessee.lessee
                    lessee.booking_credits += 1
                    lessee.save()
                    item_lessee.delete()
                item_payment_store(instance)
            else:
                raise ValidationError(
                    {"payment": ["Something went wrong with the payment"]}
                )

            instance.charge_id = charge.id
            instance.save()
            return Response({"detail": "Payment done"})

    @action(detail=True, methods=["post"], permission_classes=(IsAuthenticated,))
    def transfert_funds(self, request, slug=None):
        customer = self.request.user
        instance = get_object_or_404(Item.objects.filter(customer=customer), slug=slug)

        stripe.api_key = settings.STRIPE_PRIVATE_KEY
        amount = int(instance.price * 100 * 90 / 100)

        if instance.creator.stripe_user_id:
            transfert = stripe.Transfer.create(
                amount=amount,
                currency="cad",
                source_transaction=instance.charge_id,
                destination=instance.creator.stripe_user_id,
            )

            if transfert:
                item_transfert_funds(instance)
        else:
            raise ValidationError(
                {"payment": ["Something went wrong with the payment"]}
            )

        instance.is_item_received = True
        instance.save()

        return Response({"detail": "Confirmation Received"})


class ItemLesseeViewSet(ModelViewSet):
    permission_classes = (ItemLesseePermission,)
    pagination_class = ItemPagination

    def get_queryset(self):
        return [
            item_lessee.item for item_lessee in self.request.user.item_lessees.all()
        ]

    def get_serializer_class(self):
        if self.request.method == "POST":
            return ItemLesseeSerializer
        else:
            return ItemListSerializer

    def perform_create(self, serializer):
        lessee = self.request.user

        if Item.objects.filter(pk=self.request.data["item"], creator=lessee).exists():
            raise ValidationError({"booking": ["You cannot book your own item."]})

        if 0 < lessee.booking_credits:
            lessee.booking_credits -= 1
        else:
            raise ValidationError(
                {"booking": ["You don't have booking credits anymore. Top up, please."]}
            )
        serializer.save(lessee=lessee)
        lessee.save()

        itemLessee = ItemLessee.objects.get(
            lessee=lessee, item__pk=self.request.data["item"]
        )
        item_booking_notification(itemLessee)

    @action(detail=True, methods=["get"])
    def dates(self, request, pk=None):

        if self.request.user and self.request.user.is_authenticated:
            serializer = ItemLesseeOwnerSerializer(
                Item.objects.get(pk=pk).item_lessees.all(), many=True
            )
        else:
            serializer = ItemLesseeSerializer(
                Item.objects.get(pk=pk).item_lessees.all(), many=True
            )
        return Response(serializer.data)

    @action(detail=False, methods=["get"])
    def date(self, request, pk=None):
        item_pk = int(request.GET["item"])

        lessee = self.request.user
        if ItemLessee.objects.filter(lessee=lessee, item__pk=item_pk).exists():
            serializer = ItemLesseeSerializer(
                ItemLessee.objects.get(lessee=lessee, item__pk=item_pk)
            )
            return Response(serializer.data)
        return Response({"id": None})


class ItemPictureViewSet(ModelViewSet):
    serializer_class = ItemPictureSerializer
    permission_classes = (ItemPicturePermission,)

    def get_queryset(self):
        if (
            self.request.user
            and self.request.user.is_authenticated
            and self.request.method == "GET"
            and "slug" in self.request.GET
        ):
            owned_items_id = self.request.user.owned_items.filter(
                slug=self.request.GET["slug"]
            ).values_list("pk", flat=True)
            return ItemPicture.objects.filter(item__pk__in=owned_items_id)
        else:
            owned_items_id = self.request.user.owned_items.values_list("pk", flat=True)
            return ItemPicture.objects.filter(item__pk__in=owned_items_id)


def unique_slug_generator(title):
    slug = slugify(f"{title}-{uuid.uuid4().hex[:10]}")
    while Item.objects.filter(slug=slug).exists():
        slug = slugify(f"{title}-{uuid.uuid4().hex[:10]}")
    return slug
