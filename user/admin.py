from django.contrib import admin
from .models import User, Contact, Rating


class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "username")


class ContactAdmin(admin.ModelAdmin):
    list_display = ("id", "email")


class RatingAdmin(admin.ModelAdmin):
    list_disday = ("id", "user_target")


admin.site.register(User, UserAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Rating, RatingAdmin)
