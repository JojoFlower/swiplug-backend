# Generated by Django 2.1.7 on 2019-04-04 12:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("user", "0001_initial")]

    operations = [
        migrations.AddField(
            model_name="user",
            name="address",
            field=models.CharField(blank=True, max_length=512),
        ),
        migrations.AddField(
            model_name="user",
            name="address_lat",
            field=models.DecimalField(
                blank=True, decimal_places=20, max_digits=25, null=True
            ),
        ),
        migrations.AddField(
            model_name="user",
            name="address_lng",
            field=models.DecimalField(
                blank=True, decimal_places=20, max_digits=25, null=True
            ),
        ),
        migrations.AddField(
            model_name="user",
            name="birthday",
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="user",
            name="gender",
            field=models.CharField(
                blank=True, choices=[("Mr", "Mr"), ("Ms", "Ms")], max_length=2
            ),
        ),
        migrations.AddField(
            model_name="user",
            name="phone_number",
            field=models.CharField(blank=True, max_length=16),
        ),
        migrations.AddField(
            model_name="user",
            name="professional_category",
            field=models.CharField(
                blank=True,
                choices=[
                    ("Executive", "Executive"),
                    ("Liberal Profession", "Liberal Profession"),
                    ("Self-employed", "Self-employed"),
                    ("Technician", "Technician"),
                    ("Teacher", "Teacher"),
                    ("Craftsman", "Craftsman"),
                    ("Office Worker", "Office Worker"),
                    ("Personal Service", "Personal Service"),
                    ("Merchant", "Merchant"),
                    ("Workman", "Workman"),
                    ("Pensioner", "Pensioner"),
                    ("Student", "Student"),
                    ("Unemployed", "Unemployed"),
                    ("Homemaker", "Homemaker"),
                    ("Soldier", "Soldier"),
                ],
                max_length=32,
            ),
        ),
        migrations.AddField(
            model_name="user",
            name="profile_picture",
            field=models.ImageField(blank=True, null=True, upload_to="profile_pics"),
        ),
    ]
