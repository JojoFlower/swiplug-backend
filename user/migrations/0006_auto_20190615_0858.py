# Generated by Django 2.2.2 on 2019-06-15 08:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("user", "0005_auto_20190609_1247")]

    operations = [
        migrations.AlterField(
            model_name="user",
            name="email",
            field=models.EmailField(
                max_length=254, unique=True, verbose_name="email address"
            ),
        )
    ]
