# Generated by Django 2.2.4 on 2019-08-21 13:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("user", "0010_user_is_first_offer_submit")]

    operations = [
        migrations.AddField(
            model_name="user",
            name="booking_credits",
            field=models.PositiveSmallIntegerField(default=0),
        )
    ]
