# Generated by Django 2.2.4 on 2019-09-11 20:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("user", "0013_rating")]

    operations = [
        migrations.RemoveField(model_name="rating", name="mark"),
        migrations.AddField(
            model_name="rating",
            name="communication_mark",
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="rating",
            name="item_condition_mark",
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="rating",
            name="payment_equity_mark",
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="rating",
            name="timeliness_mark",
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
