from django.db.models.signals import pre_save, pre_delete
from rest_framework.exceptions import ValidationError
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver
from django.db import models


class User(AbstractUser, models.Model):
    GENDER = (("Mr", "Mr"), ("Ms", "Ms"))
    gender = models.CharField(max_length=2, choices=GENDER, blank=True)
    email = models.EmailField(unique=True, verbose_name="email address")
    profile_picture = models.ImageField(upload_to="profile_pics", null=True, blank=True)
    address = models.CharField(max_length=512, blank=True)
    address_details = models.CharField(max_length=512, blank=True)
    phone_number = models.CharField(max_length=16, blank=True)
    birthday = models.DateField(null=True, blank=True)
    stripe_user_id = models.CharField(max_length=512, blank=True)
    company_name = models.CharField(max_length=512, blank=True)
    company_address = models.CharField(max_length=512, blank=True)
    company_phone = models.CharField(max_length=16, blank=True)
    is_first_offer_submit = models.BooleanField(default=False)
    booking_credits = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.email


class Contact(models.Model):
    email = models.EmailField(unique=True, verbose_name="email address")

    def __str__(self):
        return self.email


class Rating(models.Model):
    user_target = models.ForeignKey(
        User, related_name="user_ratings", on_delete=models.CASCADE
    )
    user_creator = models.ForeignKey(
        User,
        related_name="given_ratings",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    item = models.ForeignKey(
        "item.Item",
        related_name="related_ratings",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    communication_mark = models.PositiveSmallIntegerField(blank=True, null=True)
    timeliness_mark = models.PositiveSmallIntegerField(blank=True, null=True)
    item_condition_mark = models.PositiveSmallIntegerField(blank=True, null=True)
    payment_equity_mark = models.PositiveSmallIntegerField(blank=True, null=True)
    comments = models.CharField(max_length=1024, blank=True)

    class Meta:
        unique_together = ("user_target", "user_creator", "item")

    def __str__(self):
        return f"{self.item}_{self.user_creator}"

    def save(self, *args, **kwargs):
        if (
            10 < self.communication_mark
            or 10 < self.timeliness_mark
            or 10 < self.item_condition_mark
            or 10 < self.payment_equity_mark
        ):
            raise ValidationError({"mark": "Incorrect mark"})
        super().save(*args, **kwargs)


@receiver(pre_save, sender=User)
def delete_s3_image_save(sender, instance, **kwargs):
    if sender.objects.filter(pk=instance.pk).exists():
        obj = sender.objects.get(pk=instance.pk).profile_picture
        if obj and obj != instance.profile_picture:
            storage, name = (obj.storage, obj.name)
            storage.delete(name)


@receiver(pre_delete, sender=User)
def delete_s3_image_delete(sender, instance, **kwargs):
    obj = instance.profile_picture
    if obj:
        storage, name = (obj.storage, obj.name)
        storage.delete(name)
