from rest_auth.serializers import PasswordResetSerializer, UserDetailsSerializer
from rest_auth.registration.serializers import RegisterSerializer
from django.contrib.auth.forms import PasswordResetForm
from rest_framework import serializers
from .models import Contact, Rating


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("id", "email")
        model = Contact


class CustomRegisterSerializer(RegisterSerializer):
    first_name = serializers.CharField(max_length=30)
    last_name = serializers.CharField(max_length=150)

    def get_cleaned_data(self):
        return {
            "username": self.validated_data.get("username", ""),
            "password1": self.validated_data.get("password1", ""),
            "email": self.validated_data.get("email", ""),
            "first_name": self.validated_data.get("first_name", ""),
            "last_name": self.validated_data.get("last_name", ""),
        }


class CustomUserDetailsSerializer(UserDetailsSerializer):
    class Meta(UserDetailsSerializer.Meta):
        fields = UserDetailsSerializer.Meta.fields + (
            "gender",
            "profile_picture",
            "address",
            "address_details",
            "phone_number",
            "birthday",
            "company_name",
            "company_address",
            "company_phone",
            "is_first_offer_submit",
            "booking_credits",
        )
        read_only_fields = ("booking_credits",)


class CustomPasswordResetSerializer(PasswordResetSerializer):
    class CustomPasswordResetForm(PasswordResetForm):
        def send_mail(
            self,
            subject_template_name,
            email_template_name,
            context,
            from_email,
            to_email,
            html_email_template_name=None,
        ):
            super().send_mail(
                subject_template_name,
                email_template_name,
                context,
                from_email,
                to_email,
                html_email_template_name="registration/password_reset_email.html",
            )

    password_reset_form_class = CustomPasswordResetForm


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            "id",
            "user_target",
            "user_creator",
            "item",
            "communication_mark",
            "item_condition_mark",
            "payment_equity_mark",
            "timeliness_mark",
            "comments",
        )
        model = Rating
        read_only_fields = ("user_creator",)
