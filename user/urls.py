from django.urls import include, path
from rest_auth.registration.views import VerifyEmailView
from rest_framework import routers
from django.views.generic import TemplateView
from .views import AccountDeleteView, AccountStripeView, ContactViewSet, RatingViewSet

router = routers.DefaultRouter()
router.register(r"contacts", ContactViewSet, basename="ContactViewSet")
router.register(r"ratings", RatingViewSet, basename="RatingViewSet")

urlpatterns = [
    path(
        "password-reset/confirm/<uidb64>/<token>/",
        TemplateView.as_view(template_name="password_reset_email.html"),
        name="password_reset_confirm",
    ),
    path("rest-auth/", include("rest_auth.urls")),
    path("rest-auth/registration/", include("rest_auth.registration.urls")),
    path(
        "rest-auth/account-confirm-email/",
        VerifyEmailView.as_view(),
        name="account_email_verification_sent",
    ),
    path(
        "rest-auth/account-delete/",
        AccountDeleteView.as_view({"delete": "destroy"}),
        name="account_delete",
    ),
    path(
        "rest-auth/stripe-get/",
        AccountStripeView.as_view({"get": "retrieve"}),
        name="account_stripe_update",
    ),
    path(
        "rest-auth/stripe-update/",
        AccountStripeView.as_view({"post": "update"}),
        name="account_stripe_update",
    ),
    path("", include(router.urls)),
]
