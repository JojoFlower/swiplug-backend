import json
import stripe
import requests
from .serializers import ContactSerializer, RatingSerializer
from .models import Contact, Rating
from django.conf import settings
from rest_framework.decorators import action
from rest_framework.viewsets import ViewSet, ModelViewSet
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated, BasePermission


class PostOnlyPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method == "POST":
            return True
        return False


class RatingPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user and request.user.is_authenticated:
            if request.method in ["GET", "POST"]:
                return True
        return False


class ContactViewSet(ModelViewSet):
    permission_classes = (PostOnlyPermission,)
    serializer_class = ContactSerializer
    queryset = Contact.objects.none()


class AccountDeleteView(ViewSet):
    permission_classes = (IsAuthenticated,)

    def destroy(self, request, pk=None):
        request.user.delete()
        return Response({"detail": "User deleted"})


class AccountStripeView(ViewSet):
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, pk=None):
        stripe_user_id = request.user.stripe_user_id

        if stripe_user_id:
            stripe.api_key = settings.STRIPE_PRIVATE_KEY
            account = stripe.Account.retrieve(stripe_user_id)
            link = account.login_links.create()
            return Response({"stripe_url": link["url"]})

        return Response({"stripe_url": None})

    def update(self, request, pk=None):
        user = request.user

        headers = {"Accept": "application/json", "Content-Type": "application/json"}

        payload = {
            "client_secret": settings.STRIPE_PRIVATE_KEY,
            "code": request.data["code"],
            "grant_type": "authorization_code",
        }

        request_url = "https://connect.stripe.com/oauth/token"
        response = requests.post(url=request_url, headers=headers, json=payload)
        content = json.loads(response._content)

        if response.status_code != 200:
            raise ValidationError({"user": ["Something went wrong"]})

        user.stripe_user_id = content["stripe_user_id"]
        user.save()

        return Response({"detail": "Profile updated"})


class RatingViewSet(ModelViewSet):
    permission_classes = (RatingPermission,)
    serializer_class = RatingSerializer
    queryset = Rating.objects.none()

    def perform_create(self, serializer):
        user_creator = self.request.user
        serializer.save(user_creator=user_creator)

    @action(detail=False, methods=["get"])
    def is_comment(self, request, pk=None):
        item_pk = int(request.GET["item"])
        user_target_pk = (
            int(request.GET["user_target"])
            if request.GET["user_target"] != "null"
            else None
        )
        user_creator = self.request.user
        is_comment = (
            Rating.objects.filter(
                item__pk=item_pk,
                user_target__pk=user_target_pk,
                user_creator=user_creator,
            ).exists()
            if user_target_pk
            else False
        )

        return Response({"is_comment": is_comment})
